package jsend

import "testing"
import "github.com/stretchr/testify/assert"

type ParameterTest struct {
	Description  string
	Status       string
	Data         interface{}
	Message      string
	Expected     bool
	ErrorMessage string
	Response     string
}

var tests = [...]ParameterTest{
	{"Should fail if status is success but no data present", "success", nil, "", false, "data can't be empty when status is set to success!", ""},
	{"Should succeed if status is success and data present", "success", "data", "", true, "", `{"status": "success", "data":"data"}`},
	{"Should fail if status is error but no message present", "error", nil, "", false, "message can't be empty when status is set to error!", ""},
	{"Should succeed if status is error and message present", "error", nil, "message", true, "", `{"status":"error", "message":"message"}`},
	{"Should fail if status is fail but no data present", "fail", nil, "", false, "data can't be empty when status is set to fail!", ""},
	{"Should succeed if status is fail and data present", "fail", "data", "", true, "", `{"status":"fail", "data":"data"}`},
	{"Should fail if status isn't success, fail or error", "invalid_status", nil, "", false, "Invalid value for status. Must be success, error or fail!", ""},
}

func TestFormatJSendReponse(t *testing.T) {
	for _, test := range tests {
		j := &JSendResponse{test.Status, test.Data, test.Message, 0}
		resp, err := FormatJSendResponse(j)
		assert.Equal(t, err == nil, test.Expected, test.Description)
		if err == nil {
			assert.JSONEq(t, test.Response, resp, test.Description)
		}

	}
}
