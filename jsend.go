package jsend

import "encoding/json"
import "errors"

type JSendResponse struct {
	Status  string      `json:"status,omitempty"`
	Data    interface{} `json:"data,omitempty"`
	Message string      `json:"message,omitempty"`
	Code    int         `json:"code,omitempty"`
}

func FormatJSendResponse(j *JSendResponse) (string, error) {
	switch j.Status {
	case "success":
		if j.Data == nil {
			return "", errors.New("data can't be empty when status is set to success!")
		}
	case "error":
		if j.Message == "" {
			return "", errors.New("message can't be empty when status is set to error!")
		}
	case "fail":
		if j.Data == nil {
			return "", errors.New("data can't be empty when status is set to fail!")
		}
	default:
		return "", errors.New("Invalid value for status. Must be success, error or fail!")
	}
	response, err := json.Marshal(j)
	return string(response[:]), err
}
